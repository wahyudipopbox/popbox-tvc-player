﻿// Decompiled with JetBrains decompiler
// Type: VideoScreensaver.Properties.Resources
// Assembly: VideoScreensaver, Version=1.0.1.0, Culture=neutral, PublicKeyToken=null
// MVID: C08998EB-033B-41A3-9CEF-B6AD2A5CCCC9
// Assembly location: D:\##GUI_PROJECT##\popboxclient_ina\player\PopBoxTVCPlayer.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace VideoScreensaver.Properties
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (VideoScreensaver.Properties.Resources.resourceMan == null)
          VideoScreensaver.Properties.Resources.resourceMan = new ResourceManager("VideoScreensaver.Properties.Resources", typeof (VideoScreensaver.Properties.Resources).Assembly);
        return VideoScreensaver.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return VideoScreensaver.Properties.Resources.resourceCulture;
      }
      set
      {
        VideoScreensaver.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
