﻿// Decompiled with JetBrains decompiler
// Type: VideoScreensaver.Properties.Settings
// Assembly: VideoScreensaver, Version=1.0.1.0, Culture=neutral, PublicKeyToken=null
// MVID: C08998EB-033B-41A3-9CEF-B6AD2A5CCCC9
// Assembly location: D:\##GUI_PROJECT##\popboxclient_ina\player\PopBoxTVCPlayer.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace VideoScreensaver.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }
  }
}
