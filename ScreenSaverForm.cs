﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;

namespace PopBoxTVCPlayer
{
    public class ScreenSaverForm : Form
    {
        private System.Drawing.Point mouseLocation;
        private bool previewMode;
        private int screenIndex;
        private ScreensaverSettings settings;
        private RoutedEventHandler videoEnded;
        private MediaElement mediaElement;
        private IContainer components;
        private ElementHost elementHost1;

        public event EventHandler ReadyToPlay;

        public ScreenSaverForm()
          : this(0, (ScreensaverSettings)null)
        {
        }

        public ScreenSaverForm(int screenIndex, ScreensaverSettings settings)
        {
            this.InitializeComponent();
            this.videoEnded = new RoutedEventHandler(this.mediaElement_MediaEnded);
            this.screenIndex = screenIndex;
            this.settings = settings;
            this.mediaElement = new MediaElement();
            this.mediaElement.MouseMove += new System.Windows.Input.MouseEventHandler(this.mediaElement_MouseMove);
            this.mediaElement.MouseDown += new MouseButtonEventHandler(this.mediaElement_MouseDown);
            this.mediaElement.LoadedBehavior = MediaState.Manual;
            this.elementHost1.Child = (UIElement)this.mediaElement;
        }

        public ScreenSaverForm(IntPtr PreviewWndHandle, ScreensaverSettings settings)
          : this(0, settings)
        {
            NativeMethods.SetParent(this.Handle, PreviewWndHandle);
            NativeMethods.SetWindowLong(this.Handle, -16, new IntPtr(NativeMethods.GetWindowLong(this.Handle, -16) | 1073741824));
            Rectangle lpRect;
            NativeMethods.GetClientRect(PreviewWndHandle, out lpRect);
            this.Size = lpRect.Size;
            this.Location = new System.Drawing.Point(0, 0);
            this.previewMode = true;
            this.LoadVideo();
            this.Start();
        }

        public void Start()
        {
            if (this.mediaElement == null)
                return;
            this.BeginInvoke((Delegate)new ScreenSaverForm.DoSomething(this.ResetPosition));
        }

        private void ResetPosition()
        {
            this.mediaElement.Play();
        }

        public void LoadVideo()
        {
            try
            {
                if (string.IsNullOrEmpty(VideoSynchronizer.Instance.CurrentMovie))
                    return;
                this.mediaElement.Stop();
                this.mediaElement.Source = new Uri(VideoSynchronizer.Instance.CurrentMovie);
                this.mediaElement.MediaEnded += this.videoEnded;

                (PresentationSource.FromVisual((Visual)this.mediaElement) as HwndSource).CompositionTarget.RenderMode = RenderMode.SoftwareOnly;
                this.mediaElement.Volume = this.screenIndex > 0 ? 0.0 : (this.settings.PlayAudio ? 1.0 : 0.0);
                if (this.ReadyToPlay == null)
                    return;
                this.ReadyToPlay((object)this, (EventArgs)null);
                settings.LogToFile(VideoSynchronizer.Instance.CurrentMovie.Replace(settings.MoviePath + "\\", ""));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to play " + VideoSynchronizer.Instance.CurrentMovie);
                Console.WriteLine(ex.StackTrace);
                if (this.screenIndex != 0)
                    return;
                VideoSynchronizer.Instance.StartScreensavers();
            }
        }

        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            this.mediaElement.MediaEnded -= this.videoEnded;
            if (this.screenIndex != 0)
                return;
            VideoSynchronizer.Instance.StartScreensavers();
        }

        private void ScreenSaverForm_Load(object sender, EventArgs e)
        {
            if (this.previewMode)
                return;
            this.Bounds = Screen.AllScreens[this.screenIndex].Bounds;
            System.Windows.Forms.Cursor.Hide();
            this.TopMost = true;
        }

        private void mediaElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.KillApp();
        }

        private void mediaElement_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            System.Drawing.Point position = System.Windows.Forms.Cursor.Position;
            if (!this.mouseLocation.IsEmpty && Math.Max(Math.Abs(position.X - this.mouseLocation.X), Math.Abs(position.Y - this.mouseLocation.Y)) > 5)
                this.KillApp();
            this.mouseLocation = position;
        }

        private void KillApp()
        {
            if (this.previewMode)
                return;
            System.Windows.Forms.Application.Exit();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            this.HandleKey(keyData);
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public void HandleKey(Keys keyData)
        {
            Console.WriteLine(keyData.ToString());
            if (keyData == Keys.N)
            {
                if (this.mediaElement == null)
                    return;
                VideoSynchronizer.Instance.StartScreensavers();
            }
            else
                this.KillApp();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.elementHost1 = new ElementHost();
            this.SuspendLayout();
            this.elementHost1.Dock = DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(284, 262);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = (UIElement)null;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add((System.Windows.Forms.Control)this.elementHost1);
            this.FormBorderStyle = FormBorderStyle.None;
            this.Name = nameof(ScreenSaverForm);
            this.Text = "Form1";
            this.Load += new EventHandler(this.ScreenSaverForm_Load);
            this.ResumeLayout(false);
        }



        private delegate void DoSomething();
    }
}
