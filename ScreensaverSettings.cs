﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO;
using System.Windows.Forms.Design;

namespace PopBoxTVCPlayer
{
    public class ScreensaverSettings
    {
        private static readonly string registryRoot = "HKEY_CURRENT_USER\\Software\\PopBox\\PopBoxTVCPLayer";

        public bool PlayAudio { get; set; }

        [Editor(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string MoviePath { get; set; }

        public void Save()
        {
            Registry.SetValue(ScreensaverSettings.registryRoot, "PlayAudio", (object)this.PlayAudio, RegistryValueKind.String);
            Registry.SetValue(ScreensaverSettings.registryRoot, "MoviePath", (object)this.MoviePath, RegistryValueKind.String);
        }

        public void Load()
        {
            string str1 = Registry.GetValue(ScreensaverSettings.registryRoot, "PlayAudio", (object)this.PlayAudio) as string;
            if (!string.IsNullOrEmpty(str1))
                this.PlayAudio = bool.Parse(str1);
            string str2 = Registry.GetValue(ScreensaverSettings.registryRoot, "MoviePath", (object)this.MoviePath) as string;
            if (string.IsNullOrEmpty(str2))
                return;
            this.MoviePath = str2;
        }

        public DateTime GetUTCNowToTimezone(string timezone)
        {
            DateTime output = DateTime.UtcNow;
            if (timezone != "00:00:00")
            {
                string[] aTo = timezone.Split(':');
                output = output.AddHours(Convert.ToDouble(aTo[0]));
                output = output.AddMinutes(Convert.ToDouble(aTo[1]));
            }
            return output;
        }

        public void LogToFile(string txt)
        {
            string todayDate = this.GetUTCNowToTimezone("07:00:00").ToString("yyyy-MM-dd HH:mm:ss");
            string logFile = Directory.GetCurrentDirectory() + "\\last_history.log";
            /*if (this.MoviePath != "")
            {
                logFile = this.MoviePath + "\\last_history.log";
            }*/

            using (StreamWriter w = File.CreateText(logFile))
            {
                //w.WriteLine("[" + todayDate + "] " + txt);
                w.WriteLine(txt);
                w.Close();
            }
        }
    }
}
